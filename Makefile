CC=g++
LIBS=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

cchess:
	   @$(CC) game.cpp figure.cpp chessboard.cpp -g3 -o game $(LIBS)
	   @./game
	   @rm -f game