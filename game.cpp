#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <cstdio>
#include <iostream>
#include "figure.h"
#include "chessboard.h"
#include <SFML/Graphics.hpp>
#define BOARD_SIZE 1000 //DO NOT CHANGE
#define FIG_SIZE 101
#define H1_X 806
#define H1_Y 806

bool within_bounds(int x, int y){
    if(x < 0 || y < 0 || x > 7 || y > 7) return 0;
    return 1;
}

int abs(int x){
    return x < 0 ? -x : x;
}

Tile* find_king(ChessBoard *chessboard, Color c){
    Tile* current;
    ChessBoard tmp_board;
    tmp_board.copy_board(chessboard);
    for(int i = 0; i < Max_tile; ++i){
        for(int j = 0; j < Max_tile; ++j){
            current = tmp_board.getTile(i, j);
            if(current->getFigure() != nullptr){
                // printf("%d %d - na polu jest %d\n", current->getX(), current->getY(), current->getFigure()->getType());
            }
            if(current->getFigure() != nullptr && current->getFigure()->getType() == T_King && current->getFigure()->getColor() == c){
                printf("Sprawdzamy tile'a dla %d, %d\n", i, j);
                current = tmp_board.getTile(i, j);
                printf("Skoro tak, to zwracam tile'a o kordach %d %d\n", current->getX(), current->getY());
                return current;
            }
        }
    }
    return nullptr;
}

int* find_king_coordinates(ChessBoard *chessboard, Color c){
    int *result = (int*)malloc(sizeof(int) * 2);
    Tile* current;
    ChessBoard tmp_board;
    tmp_board.copy_board(chessboard);
    for(int i = 0; i < Max_tile; ++i){
        for(int j = 0; j < Max_tile; ++j){
            current = tmp_board.getTile(i, j);
            if(current->getFigure() != nullptr){
                // printf("%d %d - na polu jest %d\n", current->getX(), current->getY(), current->getFigure()->getType());
            }
            if(current->getFigure() != nullptr && current->getFigure()->getType() == T_King && current->getFigure()->getColor() == c){
                printf("Sprawdzamy tile'a dla %d, %d\n", i, j);
                // printf("Skoro tak, to zwracam tile'a o kordach %d %d\n", current->getX(), current->getY());
                result[0] = i;
                result[1] = j;
                return result;
            }
        }
    }
    result[0] = -1;
    result[0] = -1;
    return nullptr;
}

bool is_mate(ChessBoard *chessboard, Color c){
    Figure* current;
    ChessBoard tmp_board;
    for(int i = 0; i < Max_tile; ++i){
        for(int j = 0; j < Max_tile; ++j){
            current = chessboard->getTile(i, j)->getFigure();
            if(current != nullptr && current->getColor() == c){
                std::vector<std::pair<int,int>> tmp = current->possibleMoves(i, j, chessboard);
                Tile* king_tile;
                for(auto &t: tmp){
                    tmp_board.copy_board(chessboard);
                    tmp_board.getTile(t.first, t.second)->setFigure(tmp_board.getTile(i, j)->getFigure());
                    tmp_board.getTile(i, j)->setFigure(nullptr);
                    king_tile = find_king(&tmp_board, c);
                    if(!king_tile->getFigure()->isCheckedNow(king_tile->getX(), king_tile->getY(), &tmp_board)){
                        // printf("To nie jest mat, bo %d moze sie ruszyc na %d %d\n", current->getType(), i, j);
                        return 0;
                    }
                }
            }
        }
    }
    return 1;
}

void draw_button(sf::RenderWindow target_window, sf::Texture texture, int x, int y){
    sf::Sprite button;
    button.setTexture(texture);
    button.setPosition(x, y);
    target_window.draw(button);
}

int main(){

    sf::RenderWindow window(sf::VideoMode(BOARD_SIZE + 200, BOARD_SIZE - 10), "CChess!");
    sf::Texture true_background, background, new_game_button, currently_playing, white_playing, black_playing;
    sf::Texture wking, wqueen, wrook, wbishop, wknight, wpawn, bking, bqueen, brook, bbishop, bknight, bpawn;

    true_background.loadFromFile("assets/true_background.jpg");
    background.loadFromFile("assets/chessboard.jpg");
    new_game_button.loadFromFile("assets/new_game_button.png");
    currently_playing.loadFromFile("assets/currently_playing.png");
    white_playing.loadFromFile("assets/white.png");
    black_playing.loadFromFile("assets/black.png");
    wking.loadFromFile("assets/whiteking.png"); 
    wqueen.loadFromFile("assets/whitequeen.png");
    wrook.loadFromFile("assets/whiterook.png");
    wbishop.loadFromFile("assets/whitebishop.png");
    wknight.loadFromFile("assets/whiteknight.png");
    wpawn.loadFromFile("assets/whitepawn.png");
    bking.loadFromFile("assets/blackking.png");
    bqueen.loadFromFile("assets/blackqueen.png");
    brook.loadFromFile("assets/blackrook.png");
    bbishop.loadFromFile("assets/blackbishop.png");
    bknight.loadFromFile("assets/blackknight.png");
    bpawn.loadFromFile("assets/blackpawn.png");

    sf::Sprite true_background_sprite(true_background);
    sf::Sprite Board(background);
    sf::Sprite Piece;
    sf::Sprite button;

    Piece.setScale(0.33, 0.33);
    true_background_sprite.setPosition(BOARD_SIZE - 300, 0);
    true_background_sprite.setScale(1.2, 10);
    button.setScale(0.85, 0.85);

    ChessBoard chessboard;
    int source_x = -1, source_y = -1, destination_x, destination_y;
    int legal_move = 1;
    sf::Vector2i cursor;
    Color whose_move = White;
    // int white_king_x = 0, white_king_y = 3, black_king_x = 7, black_king_y = 3;
    ChessBoard tmp_board;
    while(window.isOpen()){
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed)
                window.close();
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                cursor = sf::Mouse::getPosition(window);
                printf("Kursor jest na: %d, %d\n", cursor.x, cursor.y);
                if(cursor.x > 1010 && cursor.x < 1180 && cursor.y > 150 && cursor.y < 220){
                    chessboard.chessboardReset();
                    whose_move = White;
                    source_x = -1, source_y = -1;
                    continue;
                }
                if(source_x == -1 && source_y == -1){
                    source_x = 7 - ((cursor.y / FIG_SIZE) - 1);
                    source_y = 7 - (cursor.x / FIG_SIZE) + 1; //if anyone touches this i will not be happy
                                                              //this made me come to the conclusion that
                                                              //the chessboard module needs a revamp eventually
                                                              //why do we even assume the H1 square as point [0, 0]!?
                    printf("Nacisk na polu %d, %d\n", source_x, source_y);
                    if(within_bounds(source_x, source_y)){
                        Figure *figure_in_question = chessboard.getTile(source_x, source_y)->getFigure();
                        if(figure_in_question == nullptr || figure_in_question->getColor() != whose_move){
                            source_x = -1, source_y = -1;
                        }
                    }
                }
                else{
                    destination_x = 7 - ((cursor.y / FIG_SIZE) - 1);
                    destination_y = 7 - (cursor.x / FIG_SIZE) + 1;
                    if(source_x == destination_x && source_y == destination_y){
                        continue;
                    }
                    printf("Nacisk wynikowy na polu %d, %d\n", destination_x, destination_y);
                    std::vector<std::pair<int,int>> tmp = chessboard.getTile(source_x, source_y)->getFigure()->possibleMoves(source_x, source_y, &chessboard);
                    legal_move = 0;
                    Tile* king_tile;
                    for(auto &t: tmp){
                        tmp_board.copy_board(&chessboard);
                        Tile *tmp_destination = tmp_board.getTile(destination_x, destination_y);
                        Tile *tmp_source = tmp_board.getTile(source_x, source_y);
                        tmp_destination->setFigure(tmp_board.getTile(source_x, source_y)->getFigure());
                        tmp_destination->setX(destination_x);
                        tmp_destination->setY(destination_y);
                        tmp_source->setFigure(nullptr);
                        tmp_source->setX(source_x);
                        tmp_source->setY(source_y);
                        king_tile = find_king(&chessboard, whose_move);
                        printf("Gracz na ruchu to %d, jego krol jest na pozycji %d %d\n", whose_move, king_tile->getX(), king_tile->getY());
                        if(t.first == destination_x && t.second == destination_y && !king_tile->getFigure()->isCheckedNow(king_tile->getX(), king_tile->getY(), &tmp_board)){
                            printf("Legalny ruch: %d, %d\n", t.first, t.second);
                            legal_move = 1;
                            break;
                        }
                    }
                    if(within_bounds(source_x, source_y) && within_bounds(destination_x, destination_y) && legal_move){
                        Tile* destination_tile = chessboard.getTile(destination_x, destination_y); //tantalskie męki
                        Tile* source_tile = chessboard.getTile(source_x, source_y);
                        destination_tile->setFigure(source_tile->getFigure());
                        destination_tile->setX(destination_x);
                        destination_tile->setY(destination_y);
                        source_tile->setFigure(nullptr);
                        source_tile->setX(source_x);
                        source_tile->setY(source_y);
                        printf("Destination tile: %d %d, posiada %d\n", destination_tile->getX(), destination_tile->getY(), destination_tile->getFigure()->getType());
                        Figure* tmp = chessboard.getTile(destination_x, destination_y)->getFigure();
                        if(tmp->getType() == T_Pawn || tmp->getType() == T_Rook || tmp->getType() == T_King){
                            printf("Jest to pion lub wieza lub krol\n");
                            tmp->nMove();
                            if(tmp->getType() == T_King){
                                // if(whose_move == White){
                                //     white_king_x = destination_x;
                                //     white_king_y = destination_y;
                                // }
                                // else{
                                //     black_king_x = destination_x;
                                //     black_king_y = destination_y;
                                // }
                                if(abs(destination_y - source_y) == 2){
                                    int row;
                                    if(tmp->getColor() == White) 
                                        row = 0;
                                    else 
                                        row = 7;
                                    if(destination_y - source_y == -2){
                                        chessboard.getTile(row, 2)->setFigure(chessboard.getTile(row, 0)->getFigure());
                                        chessboard.getTile(row, 0)->setFigure(nullptr);
                                    }
                                    else{
                                        chessboard.getTile(row, 4)->setFigure(chessboard.getTile(row, 7)->getFigure());
                                        chessboard.getTile(row, 7)->setFigure(nullptr);
                                    }
                                }
                            }
                        }
                        if(whose_move == White){
                            whose_move = Black;
                        }
                        else{
                            whose_move = White;
                        }
                    }
                    if(is_mate(&chessboard, whose_move)){
                        printf("GG!!! Wygral %d\n", !whose_move);
                    }
                    source_x = -1, source_y = -1;
                }
            }
        }
        window.clear();
        window.draw(true_background_sprite);
        window.draw(Board);

        button.setTexture(new_game_button);
        button.setPosition(BOARD_SIZE + 10, 150);
        window.draw(button);

        button.setTexture(currently_playing);
        button.setPosition(BOARD_SIZE + 15, 250);
        window.draw(button);

        if(whose_move == White){
            button.setTexture(white_playing);
            button.setPosition(BOARD_SIZE + 15, 300);
            window.draw(button);
        }
        else{
            button.setTexture(black_playing);
            button.setPosition(BOARD_SIZE + 15, 300);
            window.draw(button);
        }

        for(int i = 0; i < 8; ++i){
            for(int j = 0; j < 8; ++j){
                Figure *tmp = chessboard.getTile(i, j)->getFigure();
                if(tmp == nullptr){
                    continue;
                }
                Type typ = tmp->getType();
                Color color = tmp->getColor();
                switch(typ){
                    case T_King:
                        if(color == White){
                            Piece.setTexture(wking);
                        }
                        else{
                            Piece.setTexture(bking);
                        }
                        break;
                    case T_Queen:
                        if(color == White){
                            Piece.setTexture(wqueen);
                        }
                        else{
                            Piece.setTexture(bqueen);
                        }
                        break;
                    case T_Rook:
                        if(color == White){
                            Piece.setTexture(wrook);
                       
                        }
                        else{
                            Piece.setTexture(brook);
                        }
                        break;
                    case T_Bishop:
                        if(color == White){
                            Piece.setTexture(wbishop);
                        }
                        else{
                            Piece.setTexture(bbishop);
                        }
                        break;
                    case T_Knight:
                        if(color == White){
                            Piece.setTexture(wknight);
                        }
                        else{
                            Piece.setTexture(bknight);
                        }
                        break;
                    case T_Pawn:
                        if(color == White){
                            Piece.setTexture(wpawn);
                        }
                        else{
                            Piece.setTexture(bpawn);
                        }
                        break;
                    default:
                        break;
                }
                Piece.setPosition(H1_Y - j * FIG_SIZE, H1_X - i * FIG_SIZE);
                window.draw(Piece);

            }
        }
        window.display();
    }
    // testowanie ruchow dozwolonych figury o koordynatach (x,y)
    // x,y przydaloby sie wyluskiwac z Tile'a poniewaz to ta klasa przechowuje koordynaty
    return 0;
}
