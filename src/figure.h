#include <iostream>
#ifndef H_PIECES
#define H_PIECES

#define FIG_SIZE 101
#define BOARD_SIZE 800

typedef enum {Black, White} Color;
typedef enum {T_King, T_Queen, T_Rook, T_Bishop, T_Knight, T_Pawn, T_None} Type;
class Figure{
    private:
        Color color;
        bool killed = false;
        Type type;
    public:
        Figure(){
            this->killed = false;
            this->type = T_None;
        }
        void setColor(Color c){
            this->color = c;
        }
        Color getColor(){
            return this->color;
        }
        Figure(Color c){
            this->killed = false;
            this->color = c;
        }
        bool isKilled(){
            return this->killed;
        }
        void setToKilled(){
            this->killed = true;
        }
        void setType(Type desired){
            this->type = desired; 
        }
        Type getType(){
            return this->type;
        }
        void status(){
            std::cout<<"White: "<<this->color<<", Killed: "<<this->isKilled()<<std::endl;
        }
};
class King : public Figure{
    private:
        bool castling = false;
    public:
        void hello(){
            std::cout<<"It is good to be the king."<<std::endl;
        }
        King(Color c){
            this->setColor(c);
            this->setType(T_King);
        }
        bool isCastled(){
            return this->castling;
        }
        void setCastlingTrue(bool castlingTrue){
            this->castling = castlingTrue;
        }
};
class Queen : public Figure{
    public:
        Queen(Color c){
            this->setColor(c);
            this->setType(T_Queen);
        }
};
class Bishop : public Figure{
    public:
        Bishop(Color c){
            this->setColor(c);
            this->setType(T_Bishop);
        }
};
class Knight : public Figure{
    public:
        Knight(Color c){
            this->setColor(c);
            this->setType(T_Knight);
        }
};
class Rook : public Figure{
    public:
        Rook(Color c){
            this->setColor(c);
            this->setType(T_Rook);
        }
};
class Pawn : public Figure{
    public:
        Pawn(Color c){
            this->setColor(c);
            this->setType(T_Pawn);
        }
};
#endif