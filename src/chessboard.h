#ifndef H_CHESSBOARD
#define H_CHESSBOARD
#include <iostream>
#include "figure.h"
#include <SFML/Graphics.hpp>

class Tile{
    private:
        int x;
        int y;
        Figure* figure;
    public:
        Tile(){
            this->figure = NULL;
        }
        void setX(int x){
            this->x=x;
        }
        void setY(int y){
            this->y=y;
        }
        int getX(){
            return this->x;
        }
        int getY(){
            return this->y;
        }
        void setFigure(Figure* f){
            this->figure = f;
        }
        Figure* getFigure(){
            return this->figure;
        }
};
class ChessBoard{
    private:
        Tile** tiles;
    public:
        ChessBoard(){
            std::cout<<"Create chessboard."<<std::endl;
            this->tiles = new Tile*[8];
            for (int i=0; i<8; i++){
                this->tiles[i] = new Tile[8];
            }
            this->chessboardReset();
            std::cout<<"Chessboard reseted."<<std::endl;
        }
        ~ChessBoard(){
            for(int i=0; i<8; i++){
                delete[] this->tiles[i];
            }
            delete[] this->tiles;
            std::cout<<"Chessboard deconstructed."<<std::endl;
        }
        Tile* getTile(int x, int y){
            // if (x<0 || y<0 || x>7 || y>7) std::cout<<"Out of board!";
            return &this->tiles[x][y];
        }
        void reset(){
            // white
            // King&Queen
            this->getTile(0,4)->setFigure(new King(White));
            this->getTile(0,4)->setX(0);
            this->getTile(0,4)->setY(4);
            
            this->getTile(0,3)->setFigure(new Queen(White));
            this->getTile(0,3)->setX(0);
            this->getTile(0,3)->setY(3);
            // Bishops Knights Rooks
            for (int i=0; i<3; i++){
                if (i==0){
                    this->getTile(0,i)->setFigure(new Rook(White));
                    this->getTile(0,i)->setX(0);
                    this->getTile(0,i)->setY(i);
                    this->getTile(0,8-1-i)->setFigure(new Rook(White));
                    this->getTile(0,8-1-i)->setX(0);
                    this->getTile(0,8-1-i)->setY(8-1-i);
                }
                if (i==1){
                    this->getTile(0,i)->setFigure(new Knight(White));
                    this->getTile(0,i)->setX(0);
                    this->getTile(0,i)->setY(i);
                    this->getTile(0,8-1-i)->setFigure(new Knight(White));
                    this->getTile(0,8-1-i)->setX(0);
                    this->getTile(0,8-1-i)->setY(8-1-i);
                }
                if (i==2){
                    this->getTile(0,i)->setFigure(new Bishop(White));
                    this->getTile(0,i)->setX(0);
                    this->getTile(0,i)->setY(i);
                    this->getTile(0,8-1-i)->setFigure(new Bishop(White));
                    this->getTile(0,8-1-i)->setX(0);
                    this->getTile(0,8-1-i)->setY(8-1-i);
                }
            }
            // Pawns
            for (int i=0; i<8; i++){
                this->getTile(1,i)->setFigure(new Pawn(White));
                this->getTile(1,i)->setX(1);
                this->getTile(1,i)->setY(i);
            }
            //black
            // King&Queen
            this->getTile(7,4)->setFigure(new King(Black));
            this->getTile(7,4)->setX(7);
            this->getTile(7,4)->setY(4);
            
            this->getTile(7,3)->setFigure(new Queen(Black));
            this->getTile(7,3)->setX(7);
            this->getTile(7,3)->setY(3);
            // Bishops Knights Rooks
            for (int i=0; i<3; i++){
                if (i==0){
                    this->getTile(7,i)->setFigure(new Rook(Black));
                    this->getTile(7,i)->setX(7);
                    this->getTile(7,i)->setY(i);
                    this->getTile(7,8-1-i)->setFigure(new Rook(Black));
                    this->getTile(7,8-1-i)->setX(7);
                    this->getTile(7,8-1-i)->setY(8-1-i);
                }
                if (i==1){
                    this->getTile(7,i)->setFigure(new Knight(Black));
                    this->getTile(7,i)->setX(7);
                    this->getTile(7,i)->setY(i);
                    this->getTile(7,8-1-i)->setFigure(new Knight(Black));
                    this->getTile(7,8-1-i)->setX(7);
                    this->getTile(7,8-1-i)->setY(8-1-i);
                }
                if (i==2){
                    this->getTile(7,i)->setFigure(new Bishop(Black));
                    this->getTile(7,i)->setX(7);
                    this->getTile(7,i)->setY(i);
                    this->getTile(7,8-1-i)->setFigure(new Bishop(Black));
                    this->getTile(7,8-1-i)->setX(7);
                    this->getTile(7,8-1-i)->setY(8-1-i);
                }
            }
            // Pawns
            for (int i=0; i<8; i++){
                this->getTile(6,i)->setFigure(new Pawn(Black));
                this->getTile(6,i)->setX(6);
                this->getTile(6,i)->setY(i);
            }
        }
        void chessboardReset(){
            this->reset();
        }
        void tileInfo(int x, int y){
            std::cout<<"(x,y):("<<x<<" "<<y<<"), ";
            Figure* figure = this->getTile(x,y)->getFigure();
            if (figure != nullptr) figure->status();
            else std::cout<<"Empty tile."<<std::endl;
        }

};
#endif