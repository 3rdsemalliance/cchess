#include "chessboard.h"

ChessBoard::ChessBoard()
{
    std::cout << "Create chessboard." << std::endl;
    this->tiles = new Tile*[8];
    for (int i = 0; i < Max_tile; i++) {
        this->tiles[i] = new Tile[Max_tile];
    }
    this->chessboardReset();
    std::cout << "Chessboard reseted." << std::endl;
}
ChessBoard::~ChessBoard()
{
    for (int i = 0; i < Max_tile; i++) {
        delete[] this->tiles[i];
    }
    delete[] this->tiles;
    std::cout << "Chessboard deconstructed." << std::endl;
}
Tile* ChessBoard::getTile(int x, int y)
{
    if (x<0 || y<0 || x>7 || y>7) std::cout<<"Out of board! " << x << " " << y << std::endl;
    return &this->tiles[x][y];
}

void ChessBoard::reset()
{
    // white
    // King&Queen
    this->getTile(0, 3)->setFigure(new King(White));
    this->getTile(0, 3)->setX(0);
    this->getTile(0, 3)->setY(3);
    this->getTile(0, 4)->setFigure(new Queen(White));
    this->getTile(0, 4)->setX(0);
    this->getTile(0, 4)->setY(4);
    // Bishops Knights Rooks
    for (int i = 0; i < 3; i++) {
        if (i == 0) {
            this->getTile(0, i)->setFigure(new Rook(White));
            this->getTile(0, i)->setX(0);
            this->getTile(0, i)->setY(i);
            this->getTile(0, Max_tile - 1 - i)->setFigure(new Rook(White));
            this->getTile(0, Max_tile - 1 - i)->setX(0);
            this->getTile(0, Max_tile - 1 - i)->setY(Max_tile - 1 - i);
        }
        if (i == 1) {
            this->getTile(0, i)->setFigure(new Knight(White));
            this->getTile(0, i)->setX(0);
            this->getTile(0, i)->setY(i);
            this->getTile(0, Max_tile - 1 - i)->setFigure(new Knight(White));
            this->getTile(0, Max_tile - 1 - i)->setX(0);
            this->getTile(0, Max_tile - 1 - i)->setY(Max_tile - 1 - i);
        }
        if (i == 2) {
            this->getTile(0, i)->setFigure(new Bishop(White));
            this->getTile(0, i)->setX(0);
            this->getTile(0, i)->setY(i);
            this->getTile(0, Max_tile - 1 - i)->setFigure(new Bishop(White));
            this->getTile(0, Max_tile - 1 - i)->setX(0);
            this->getTile(0, Max_tile - 1 - i)->setY(Max_tile - 1 - i);
        }
    }
    // Pawns
    for (int i = 0; i < Max_tile; i++) {
        this->getTile(1, i)->setFigure(new Pawn(White));
        this->getTile(1, i)->setX(1);
        this->getTile(1, i)->setY(i);
    }
    // black
    //  King&Queen
    this->getTile(Max_tile - 1, 3)->setFigure(new King(Black));
    this->getTile(Max_tile - 1, 3)->setX(Max_tile - 1);
    this->getTile(Max_tile - 1, 3)->setY(3);

    this->getTile(Max_tile - 1, 4)->setFigure(new Queen(Black));
    this->getTile(Max_tile - 1, 4)->setX(Max_tile - 1);
    this->getTile(Max_tile - 1, 4)->setY(4);
    // Bishops Knights Rooks
    for (int i = 0; i < 3; i++) {
        if (i == 0) {
            this->getTile(Max_tile - 1, i)->setFigure(new Rook(Black));
            this->getTile(Max_tile - 1, i)->setX(Max_tile - 1);
            this->getTile(Max_tile - 1, i)->setY(i);
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setFigure(new Rook(Black));
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setX(Max_tile - 1);
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setY(Max_tile - 1 - i);
        }
        if (i == 1) {
            this->getTile(Max_tile - 1, i)->setFigure(new Knight(Black));
            this->getTile(Max_tile - 1, i)->setX(Max_tile - 1);
            this->getTile(Max_tile - 1, i)->setY(i);
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setFigure(new Knight(Black));
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setX(Max_tile - 1);
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setY(Max_tile - 1 - i);
        }
        if (i == 2) {
            this->getTile(Max_tile - 1, i)->setFigure(new Bishop(Black));
            this->getTile(Max_tile - 1, i)->setX(Max_tile - 1);
            this->getTile(Max_tile - 1, i)->setY(i);
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setFigure(new Bishop(Black));
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setX(Max_tile - 1);
            this->getTile(Max_tile - 1, Max_tile - 1 - i)->setY(Max_tile - 1 - i);
        }
    }
    // Pawns
    for (int i = 0; i < Max_tile; i++) {
        this->getTile(6, i)->setFigure(new Pawn(Black));
        this->getTile(6, i)->setX(6);
        this->getTile(6, i)->setY(i);
    }
    // Clearing everything else
    for(int i = 2; i < 6; ++i){
        for(int j = 0; j < 8; ++j){
            this->getTile(i, j)->setFigure(nullptr);
        }
    }
}
void ChessBoard::chessboardReset()
{
    this->reset();
}
void ChessBoard::tileInfo(int x, int y)
{
    std::cout << "(x,y):(" << x << " " << y << "), ";
    Figure* figure = this->getTile(x, y)->getFigure();
    if (figure) {
        figure->status();
        figure->hello();
    } else
        std::cout << "Empty tile." << std::endl;
}

void ChessBoard::copy_board(ChessBoard *destination_chessboard){
    for(int i = 0; i < Max_tile; ++i){
        for(int j = 0; j < Max_tile; ++j){
            this->getTile(i, j)->setFigure(destination_chessboard->getTile(i, j)->getFigure());
        }
    }
}