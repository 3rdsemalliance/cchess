#ifndef H_PIECES
#define H_PIECES
#include "tile.h"
#include <utility>
#include <vector>
class ChessBoard;

typedef enum { Black, White } Color;
typedef enum {T_King, T_Queen, T_Rook, T_Bishop, T_Knight, T_Pawn, T_None} Type;
class Figure {
private:
    Color color;
    bool killed = false;
    Type type;
public:
    bool firstMove = true;
    virtual void hello();
    Figure();
    ~Figure();
    void setColor(Color c);
    Color getColor();
    bool isWhite();
    Figure(Color c);
    bool isKilled();
    void setToKilled();
    void status();
    void setType(Type desired);
    Type getType();
    virtual void nMove();
    bool fMove();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
    virtual bool isCheckedNow(int x, int y, ChessBoard *chessboard);
};

class King : public Figure {
private:
    bool castling = false;
    bool moved = false;
    bool check = false;

public:
    virtual void hello();
    King(Color c);
    bool isCastled();
    bool isCastlingEnabled();
    bool isChecked();
    void setCheck(bool check);
    void setCastlingTrue(bool castlingTrue);
    virtual void nMove();
    bool fMove();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
    virtual bool isCheckedNow(int x, int y, ChessBoard *chessboard);
};

class Queen : public Figure {
public:
    Queen(Color c);
    virtual void hello();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
};

class Bishop : public Figure {
public:
    Bishop(Color c);
    virtual void hello();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
};

class Knight : public Figure {
public:
    Knight(Color c);
    virtual void hello();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
};

class Rook : public Figure {
public:
    Rook(Color c);
    virtual void hello();
    virtual void nMove();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
    bool fMove();
};

class Pawn : public Figure {
private:
public:
    Pawn(Color c);
    virtual void hello();
    bool fMove();
    virtual void nMove();
    virtual std::vector<std::pair<int, int>> possibleMoves(int x, int y, ChessBoard *chessboard);
};

#endif
