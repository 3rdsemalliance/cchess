# CChess

A simple game of chess, but with a twist - you can combine your pieces to gain a tactical advantage! But beware - your opponent can do that too.
