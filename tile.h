#ifndef H_TILE
#define H_TILE
#include <cstddef>
#include <iostream>

class Figure;
class Tile{
    private:
        int x;
        int y;
        Figure* figure;
    public:
        Tile(){
            this->figure = nullptr;
        }
        ~Tile(){
            
        }
        void setX(int x){
            this->x=x;
        }
        void setY(int y){
            this->y=y;
        }
        int getX(){
            return this->x;
        }
        int getY(){
            return this->y;
        }
        void setFigure(Figure* f){
            this->figure = f;
        }
        Figure* getFigure(){
            return this->figure;
        }
};
#endif
