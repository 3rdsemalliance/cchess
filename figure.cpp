#include "figure.h"
#include "chessboard.h"

void Figure::hello()
{
    std::cout << "I'm a figure." << std::endl;
}
Figure::Figure() { this->killed = false; this->type = T_None;}
Figure::~Figure() { }

void Figure::setColor(Color c)
{
    this->color = c;
}
bool Figure::isWhite()
{
    if (this->color == White)
        return true;
    else
        return false;
}
Figure::Figure(Color c)
{
    this->killed = false;
    this->color = c;
    this->firstMove = true;
}
bool Figure::isKilled()
{
    return this->killed;
}
void Figure::setToKilled()
{
    this->killed = true;
}
Color Figure::getColor(){
    return this->color;
}
void Figure::status()
{
    std::cout << "White: " << this->color << ", Killed: " << this->isKilled() << std::endl;
}
std::vector<std::pair<int, int>> Figure::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    std::vector<std::pair<int, int>> moves;
    return moves;
}
void Figure::setType(Type desired){
    this->type = desired; 
}
Type Figure::getType(){
    return this->type;
}
void Figure::nMove(){
    this->firstMove = false;
}

bool Figure::fMove(){
    return this->firstMove;
}

bool Figure::isCheckedNow(int x, int y, ChessBoard *chessboard){
    return 0;
}


void King::hello()
{
    std::cout << "It is good to be the king." << std::endl;
}
King::King(Color c)
{
    this->setColor(c);
    this->setType(T_King);
    this->setCheck(false);
}
bool King::isCastled()
{
    return this->castling;
}
bool King::isCastlingEnabled()
{
    return !this->moved;
}
bool King::isChecked()
{
    return this->check;
}
void King::setCheck(bool check)
{
    this->check = check;
}
void King::setCastlingTrue(bool castlingTrue)
{
    this->castling = castlingTrue;
}
void King::nMove()
{
    this->firstMove = false;
}

bool King::fMove(){
    return this->firstMove;
}
std::vector<std::pair<int, int>> King::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    std::vector<std::pair<int, int>> moves;
    for (int i = x - 1; i <= x + 1; i++){
        for (int j = y - 1; j <= y + 1; j++){
            // printf("(Possible_moves) %d, %d\n", i, j);
            if ((i != x || j != y) && (i >= 0 && i < Max_tile && j >= 0 && j < Max_tile)){
                if (!this->isCheckedNow(i, j, chessboard)){
                    // std::cout<<"Should create pair of coords: ("<<i<<" "<<j<<") of possible move for king."<<std::endl;
                    std::pair<int, int> t = std::make_pair(i, j);
                    moves.push_back(t);
                }
                else continue;
            }
            std::cout<<i<<" "<<j<<std::endl;
        }
    }
    if (this->isCastlingEnabled()){
        Color c = this->getColor();
        if (c == White){
            Figure* figure1 = chessboard->getTile(0,0)->getFigure();
            Figure* figure2 = chessboard->getTile(0,7)->getFigure();
            bool longCastle = true;
            bool shortCastle = true;
            if (figure1 != nullptr){
                if (figure1->getType() == T_Rook && figure1->getColor() == c && figure1->fMove() == true && this->fMove()){
                    // position 1 short white castle
                    for (int i=1; i<=2; i++){
                        if(chessboard->getTile(0,i)->getFigure() != nullptr){
                            shortCastle = false;
                        }
                        if(this->isCheckedNow(0,i, chessboard)){
                            shortCastle = false;
                        }
                    }
                    if (shortCastle){
                        std::pair<int, int> t = std::make_pair(0,1);
                        moves.push_back(t);
                    }
                }
            }
            if (figure2 != nullptr){
                if (figure2->getType() == T_Rook && figure2->getColor() == c && figure2->fMove() == true && this->fMove()){
                    // position 1 short white castle
                    for (int i=6; i>=4; i--){
                        if(chessboard->getTile(0,i)->getFigure() != nullptr){
                            longCastle = false;
                        }
                        if(this->isCheckedNow(0,i, chessboard)){
                            longCastle = false;
                        }
                    }
                    if (longCastle){
                        std::pair<int, int> t = std::make_pair(0,5);
                        moves.push_back(t);
                    }
                }
            }
        }
        else if (c == Black){
            Figure* figure1 = chessboard->getTile(7,0)->getFigure();
            Figure* figure2 = chessboard->getTile(7,7)->getFigure();
            bool longCastle = true;
            bool shortCastle = true;
            if (figure1 != nullptr){
                if (figure1->getType() == T_Rook && figure1->getColor() == c && figure1->fMove() == true && this->fMove()){
                    // position 1 short white castle
                    for (int i=1; i<=2; i++){
                        if(chessboard->getTile(7,i)->getFigure() != nullptr){
                            shortCastle = false;
                        }
                        if(this->isCheckedNow(7,i, chessboard)){
                            shortCastle = false;
                        }
                    }
                    if (shortCastle){
                        std::pair<int, int> t = std::make_pair(7,1);
                        moves.push_back(t);
                    }
                }
            }
            if (figure2 != nullptr){
                if (figure2->getType() == T_Rook && figure2->getColor() == c && figure2->fMove() == true && this->fMove()){
                    // position 1 short white castle
                    for (int i=6; i>=4; i--){
                        if(chessboard->getTile(7,i)->getFigure() != nullptr){
                            longCastle = false;
                        }
                        if(this->isCheckedNow(7,i, chessboard)){
                            longCastle = false;
                        }
                    }
                    if (longCastle){
                        std::pair<int, int> t = std::make_pair(7,5);
                        moves.push_back(t);
                    }
                }
            }
        }
    }

    return moves;
}
bool King::isCheckedNow(int x, int y, ChessBoard *chessboard)
{
    Figure* figure;
    int tmpx, tmpy;
    for (int i=0; i<4; i++){
        //pawns + initial squares for queens/bishops
        if (i==0){ // up-right
            tmpx = x+1;
            tmpy = y+1;
            if(tmpx < Max_tile && tmpy < Max_tile){
                figure = chessboard->getTile(tmpx, tmpy)->getFigure();
                if(figure != nullptr){
                    if (figure->getType() == T_Pawn && figure->getColor() != this->getColor() && this->getColor() == White){
                        this->setCheck(true);
                        return this->isChecked();
                    }
                }
            }
        }
        if (i==1){ // down-right
            tmpx = x-1;
            tmpy = y+1;
            if(tmpx >= 0 && tmpy < Max_tile){
                figure = chessboard->getTile(tmpx, tmpy)->getFigure();
                if(figure != nullptr){
                    if (figure->getType() == T_Pawn && figure->getColor() != this->getColor() && this->getColor() == Black){
                        this->setCheck(true);
                        return this->isChecked();
                    }
                }
            }
        }
        if (i==2){ // down-left
            tmpx = x-1;
            tmpy = y-1;
            if(tmpx >= 0 && tmpy >= 0){
                figure = chessboard->getTile(tmpx, tmpy)->getFigure();
                if(figure != nullptr){
                    if (figure->getType() == T_Pawn && figure->getColor() != this->getColor() && this->getColor() == Black){
                        this->setCheck(true);
                        return this->isChecked();
                    }
                }
            }
        }
        if (i==3){ // up-left
            tmpx = x+1;
            tmpy = y-1;
            if(tmpx < Max_tile && tmpy >= 0){
                figure = chessboard->getTile(tmpx, tmpy)->getFigure();
                if(figure != nullptr){
                    if (figure->getType() == T_Pawn && figure->getColor() != this->getColor() && this->getColor() == White){
                        this->setCheck(true);
                        return this->isChecked();
                    }
                }
            }
        }
        while (tmpx >= 0 && tmpy >= 0 && tmpx < Max_tile && tmpy < Max_tile){
            // printf("(is_checked_now)Now checking diagonals, tile %d %d\n", tmpx, tmpy);
            figure = chessboard->getTile(tmpx, tmpy)->getFigure();
            if (figure != nullptr){
                if ((figure->getType() == T_Bishop || figure->getType() == T_Queen) && figure->getColor() != this->getColor()){
                    // std::cout<<"Attacked by Queen or Bishop: option "<<i<<", on coords:("<<tmpx<<","<<tmpy<<")"<<std::endl;
                    this->setCheck(true);
                    return this->isChecked();
                }
                else tmpx=Max_tile+1;
            }
            if (i==0){ 
                tmpx++;
                tmpy++;
            }
            if (i==1){
                tmpx--;
                tmpy++;
            }
            if (i==2){
                tmpx--;
                tmpy--;
            }
            if (i==3){
                tmpx++;
                tmpy--;
            }
        }
    }
    for (int i=0; i<4; i++){
        if (i==0){ // up
            tmpx = x+1;
            tmpy = y;
        }
        if (i==1){ // down
            tmpx = x-1;
            tmpy = y;
        }
        if (i==2){ //right
            tmpx = x;
            tmpy = y+1;
        }
        if (i==3){ //left
            tmpx = x;
            tmpy = y-1;
        }
        while (tmpx >= 0 && tmpy >= 0 && tmpx < Max_tile && tmpy < Max_tile){
            // printf("(is_checked_now)Now checking files and ranks, tile %d %d\n", tmpx, tmpy);
            Figure* figure = chessboard->getTile(tmpx, tmpy)->getFigure();
            if (figure != nullptr){
                if ((figure->getType() == T_Queen || figure->getType() == T_Rook) && figure->getColor() != this->getColor()){
                    // std::cout<<"Attacked by Queen or Rook: option "<<i<<", on coords:("<<tmpx<<","<<tmpy<<")"<<std::endl;
                    this->setCheck(true);
                    return this->isChecked();
                }
                else tmpx=Max_tile+1;
            }
            if (i==0) tmpx++;
            if (i==1) tmpx--;
            if (i==2) tmpy++;
            if (i==3) tmpy--;
        }
    }
    for (int i=0; i<8; i++){
        if(i==0){
            tmpx = x+2;
            tmpy = y+1;
        }
        if(i==1){
            tmpx = x+1;
            tmpy = y+2;
        }
        if(i==2){
            tmpx = x-1;
            tmpy = y+2;
        }
        if (i==3){
            tmpx = x-2;
            tmpy = y+1;
        }
        if (i==4){
            tmpx = x-2;
            tmpy = y-1;
        }
        if (i==5){
            tmpx = x-1;
            tmpy = y-2;
        }
        if (i==6){
            tmpx = x+1;
            tmpy = y-2;
        }
        if (i==7){
            tmpx = x+2;
            tmpy = y-1;
        }
        if (tmpx >= 0 && tmpy >= 0 && tmpx < Max_tile && tmpy < Max_tile){
            // printf("(is_checked_now)Now checking the knight, tile %d %d\n", tmpx, tmpy);
            Figure* figure = chessboard->getTile(tmpx, tmpy)->getFigure();
            if (figure != nullptr && figure->getColor() != this->getColor() && figure->getType() == T_Knight){
                std::cout<<"Attacked by Knight: option "<<i<<", on coords:("<<tmpx<<","<<tmpy<<")"<<std::endl;
                this->setCheck(true);
                return this->isChecked();
            }
            else continue;
        }
    }
    // std::cout<<"No attack on King, passing NO CHECK!"<<std::endl;
    this->setCheck(false);
    return this->isChecked();
}

Queen::Queen(Color c)
{
    this->setColor(c);
    this->setType(T_Queen);
}
void Queen::hello()
{
    std::cout << "I'm a queen." << std::endl;
}
std::vector<std::pair<int, int>> Queen::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    std::vector<std::pair<int, int>> moves;
    for (int i = x + 1; i < Max_tile; i++) {
        if (chessboard->getTile(i, y)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
            break;
        }
    }
    for (int i = x - 1; i >= 0; i--) {
        if (chessboard->getTile(i, y)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
            break;
        }
    }
    for (int i = y + 1; i < Max_tile; i++) {
        if (chessboard->getTile(x, i)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
            break;
        }
    }
    for (int i = y - 1; i >= 0; i++) {
        if (chessboard->getTile(x, i)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
            break;
        }
    }
    int tmpx = x + 1, tmpy = y + 1;
    while (tmpx < Max_tile && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx++;
        tmpy++;
    }
    tmpx = x - 1;
    tmpy = y + 1;
    while (tmpx >= 0 && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx--;
        tmpy++;
    }
    tmpx = x - 1;
    tmpy = y - 1;
    while (tmpx >= 0 && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx--;
        tmpy--;
    }
    tmpx = x + 1;
    tmpy = y - 1;
    while (tmpx < Max_tile && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx++;
        tmpy--;
    }
    return moves;
}

Bishop::Bishop(Color c)
{
    this->setColor(c);
    this->setType(T_Bishop);
}
void Bishop::hello()
{
    std::cout << "I'm a bishop." << std::endl;
}
std::vector<std::pair<int, int>> Bishop::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    std::vector<std::pair<int, int>> moves;
    int tmpx = x + 1, tmpy = y + 1;
    while (tmpx < Max_tile && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx++;
        tmpy++;
    }
    tmpx = x - 1;
    tmpy = y + 1;
    while (tmpx >= 0 && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx--;
        tmpy++;
    }
    tmpx = x - 1;
    tmpy = y - 1;
    while (tmpx >= 0 && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx--;
        tmpy--;
    }
    tmpx = x + 1;
    tmpy = y - 1;
    while (tmpx < Max_tile && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
        if (chessboard->getTile(tmpx, tmpy)->getFigure() != nullptr)
            break;
        tmpx++;
        tmpy--;
    }
    return moves;
}

Knight::Knight(Color c)
{
    this->setColor(c);
    this->setType(T_Knight);
}
void Knight::hello()
{
    std::cout << "I'm a knight." << std::endl;
}
std::vector<std::pair<int, int>> Knight::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    std::vector<std::pair<int, int>> moves;
    int tmpx, tmpy;
    tmpx = x + 2;
    tmpy = y + 1;
    if (tmpx < Max_tile && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x + 2;
    tmpy = y - 1;
    if (tmpx < Max_tile && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x + 1;
    tmpy = y + 2;
    if (tmpx < Max_tile && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x + 1;
    tmpy = y - 2;
    if (tmpx < Max_tile && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x - 2;
    tmpy = y + 1;
    if (tmpx >= 0 && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x - 2;
    tmpy = y - 1;
    if (tmpx >= 0 && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x - 1;
    tmpy = y + 2;
    if (tmpx >= 0 && tmpy < Max_tile) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    tmpx = x - 1;
    tmpy = y - 2;
    if (tmpx >= 0 && tmpy >= 0) {
        std::pair<int, int> t = std::make_pair(tmpx, tmpy);
        moves.push_back(t);
    }
    return moves;
}

Rook::Rook(Color c)
{
    this->setColor(c);
    this->setType(T_Rook);
}
void Rook::hello()
{
    std::cout << "I'm a rook." << std::endl;
}
std::vector<std::pair<int, int>> Rook::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    std::vector<std::pair<int, int>> moves;
    for (int i = x + 1; i < Max_tile; i++) {
        if (chessboard->getTile(i, y)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
            break;
        }
    }
    for (int i = x - 1; i >= 0; i--) {
        if (chessboard->getTile(i, y)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(i, y);
            moves.push_back(t);
            break;
        }
    }
    for (int i = y + 1; i < Max_tile; i++) {
        if (chessboard->getTile(x, i)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
            break;
        }
    }
    for (int i = y - 1; i >= 0; i++) {
        if (chessboard->getTile(x, i)->getFigure() == nullptr) {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
        } else {
            std::pair<int, int> t = std::make_pair(x, i);
            moves.push_back(t);
            break;
        }
    }
    return moves;
}

bool Rook::fMove(){
    return firstMove;
}

void Rook::nMove(){
    this->firstMove = false;
}

Pawn::Pawn(Color c)
{
    this->setColor(c);
    this->setType(T_Pawn);
}
void Pawn::hello()
{
    std::cout << "I'm a pawn." << std::endl;
}
bool Pawn::fMove()
{
    return this->firstMove;
}
void Pawn::nMove()
{
    this->firstMove = false;
}
std::vector<std::pair<int, int>> Pawn::possibleMoves(int x, int y, ChessBoard *chessboard)
{
    Figure* figure, *figure2;
    std::vector<std::pair<int, int>> moves;
    if (this->getColor() == White) {
        if(x + 1 < Max_tile){
            figure = chessboard->getTile(x+1,y)->getFigure();
            if (figure == nullptr) {
                std::pair<int, int> t = std::make_pair(x+1,y);
                moves.push_back(t);
            }
        }
        if(this->fMove() && (x+2) < Max_tile){
            figure2 = chessboard->getTile(x+2,y)->getFigure();
            if (figure == nullptr && figure2 == nullptr){
                std::pair<int, int> t = std::make_pair(x+2,y);
                moves.push_back(t);
            }
        }
        if((x+1) < Max_tile && (y+1) < Max_tile){
            figure = chessboard->getTile(x+1,y+1)->getFigure();
            if (figure != nullptr) {
                std::pair<int, int> t = std::make_pair(x+1,y+1);
                moves.push_back(t);
            }
        }
        if((x+1) < Max_tile && (y-1) >= 0){
            figure = chessboard->getTile(x+1,y-1)->getFigure();
            if (figure != nullptr) {
                std::pair<int, int> t = std::make_pair(x+1,y-1);
                moves.push_back(t);
            }
        }
    } else if (this->getColor() == Black){
        if(x - 1 >= 0){
            figure = chessboard->getTile(x - 1,y)->getFigure();
            if (figure == nullptr) {
                std::pair<int, int> t = std::make_pair(x - 1, y);
                moves.push_back(t);
            }
        }
        if(this->fMove() && x - 2 >= 0){
            figure2 = chessboard->getTile(x - 2,y)->getFigure();
            if (figure == nullptr && figure2 == nullptr) {
                std::pair<int, int> t = std::make_pair(x - 2,y);
                moves.push_back(t);
            }
        }
        if(x - 1 >= 0 && y + 1 < Max_tile){
            figure = chessboard->getTile(x - 1,y + 1)->getFigure();
            if (figure != nullptr) {
                std::pair<int, int> t = std::make_pair(x - 1,y + 1);
                moves.push_back(t);
            }
        }
        if(x - 1 >= 0 && y - 1 >= 0){
            figure = chessboard->getTile(x-1,y-1)->getFigure();
            if (figure != nullptr) {
                std::pair<int, int> t = std::make_pair(x-1,y-1);
                moves.push_back(t);
            }
        }
    }
    return moves;
}
