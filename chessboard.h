#ifndef H_CHESSBOARD
#define H_CHESSBOARD
#define Max_tile 8
#include <iostream>
#include "figure.h"

class ChessBoard{
    private:
        Tile** tiles;
    public:
    ChessBoard();
    ~ChessBoard();
    Tile* getTile(int x, int y);
    void reset();
    void chessboardReset();
    void tileInfo(int x, int y);
    void copy_board(ChessBoard *destination_chessboard);
};
#endif
